const uuidv1 = require('uuid/v1');

var keyDict = global.keyDict || (global.keyDict = {})
var valueDict = global.valueDict || (global.valueDict = {})


function randomBetween(start, end){
    if(typeof start != 'number' || typeof end != 'number'){
        return null;
    }
    return start + Math.round(Math.random() * Math.abs(start - end))
}

function randomUppercase(input){
    if(typeof input != 'string'){
        return '';
    }
    let out=''
    for(let i = 0; i < input.length; i++)
        out += randomBetween(0,1) ? input[i].toUpperCase() : input[i]
    
    return out;
}

const VALID_REGEXP = /^(https?:\/\/)?([\w\.\-]+)/i
const HTTP = /^(https?:\/\/)/;

function addHttps(str){
    if(!HTTP.test(str)) return 'https://' + str;
    return str;
}

function invalidValue(string){
    if(typeof string != 'string'){
        return true;
    }
    return !VALID_REGEXP.test(string);
}

function makeKey(){
    return randomUppercase(uuidv1().split('-')[0]);
}

function uniqueKey(){
    let output = makeKey()
    while(output in keyDict){
        output = makeKey()
    }
    return output;
}

function addValue(value){
    if(typeof value != 'string'){
        return null;
    }
    if(invalidValue(value)){
        return null;
    }
    if(value in valueDict){
        return keyDict[value];
    }
    let key = uniqueKey();
    keyDict[key] = addHttps(value);
    valueDict[value] = key;
    return key;
}

function getValue(key){
    return keyDict[key];
}


module.exports = {
    addValue,
    getValue
}