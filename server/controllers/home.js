const router = new require('express').Router();
const service = require('../services/key-value');

router.get('/:hash', (req, res, next)=>{
    let redirect = null;
    if(req.params && req.params.hash){
        redirect = service.getValue(req.params.hash)
    }
    if(redirect){
        return res.redirect(redirect, 301);
    }
    return next();
});

router.get('/', (req, res, next)=>{
    return res.json({ title : 'Welcome to the api' })
});

module.exports = {
    router,
    base : '/',
    weight : 10000
};