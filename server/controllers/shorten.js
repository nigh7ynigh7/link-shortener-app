const router = new require('express').Router();
const service = require('../services/key-value');
const url = require('url')
const processUtils = require('../process-utils');

router.post('/', (req, res, next)=>{
    if(!req.body && !(req.body.location || req.body.path)){
        return res.next()
    }
    let key = service.addValue((req.body.location || req.body.path))
    if(key){
        return res.json({
            key,
            path : url.resolve(processUtils.get('FINAL_PATH', 'http://localhost:3000'), key) 
        })
    }
    res.status(400).json({ message : 'Invalid path' });
})

module.exports = {
    router,
    base : '/shorten',
    weight : 50
}