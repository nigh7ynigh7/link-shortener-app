var fs = require('fs')

let dirs = fs.readdirSync(__dirname)

let output = []

dirs.forEach(o => {
    if( o == 'index.js') return;
    let toAdd = require(`./${o}`);
    if(!toAdd.base){
        toAdd.base = o.split('.')[0];
    }
    output.push(toAdd);
});

output.sort((routeA, routeB) => {
    return routeA.weight <= routeB.weight;
})

module.exports = output;