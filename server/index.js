const express = require('express')
const bodyParser = require('body-parser')
const processUtils = require('./process-utils')

require('dotenv').config({ path : 'config.env' })

const app = express();

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const swaggerUrl = '/swagger'
app.use(swaggerUrl, swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use((req,res,next)=>{
    console.info(new Date().toISOString(), req.method.toUpperCase(), req.path);
    next();
})

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

require('./controllers').forEach(route => {
    app.use(route.base, route.router);
    console.log('Added', route.base);
})

app.get('*', (req, res, next)=>{
    res.status(404).json({message : 'Resource not found'})
})

let port = processUtils.get('PORT', 3000, Number);

app.listen(port, () => {
    const finalPath = processUtils.get('FINAL_PATH', 'http://localhost:3000');
    console.log(`Listening on port ${finalPath}`);
    console.log(`See ${finalPath}${swaggerUrl} for API usage.`);
})