const util = require('util')


function get(key, def, converter=null){
    if(typeof key != 'string' || util.isNullOrUndefined(def)){
        throw new Error('Key and default value are required.');
    }
    let output = process.env[key] || def;
    if(typeof converter == 'function'){
        output = converter(output) || output;
    }
    return output;
}

function exists(key){
    return key in process.env;
}

module.exports = {
    get,
    exists,
}